extern crate popstcl_core;
extern crate rustyline;

use rustyline::Editor;
use rustyline::completion::FilenameCompleter;

use popstcl_core::*;

use std::io;
use std::io::Write;

fn main() {
    let mut vm = basic_vm();
    let mut editor = Editor::<()>::new();
    greetings();
    loop {
        let mut input = String::new();
        loop {
            let mut next_in = match editor.readline(">>> ") {
                Ok(str) => str,
                Err(e) => {
                    println!("{}", e);
                    continue;
                },
            }.to_string();

            if let Some('\\') = next_in.chars().rev().nth(0) {
                next_in.pop();
                input.push_str(&next_in);
                input.push('\n');
            } else {
                input.push_str(&next_in);
                break;
            }
        }
        match &*input {
            "q" => break,
            "cls" => { 
                clear();
                greetings();
                continue;
            },
            _ => (),
        }

        match vm.eval_str(&input) {
            Ok(_) => (),
            Err(e) => println!("Execution Error: {:?}", e),
        }
    }
}

fn greetings() {
    println!("Popstcl REPL TEST BUILD");
}

#[cfg(windows)]
fn clear() {
    std::process::Command::new("cls").status().unwrap();
}

#[cfg(unix)]
fn clear() {
    std::process::Command::new("clear").status().unwrap();
    //println!("{}[2J", 27 as char);
}
